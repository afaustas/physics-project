CC = iccavr
LIB = ilibw
CFLAGS =  -e -D__ICC_VERSION=722 -DATtiny26  -l -g -MEnhanced 
ASFLAGS = $(CFLAGS) 
LFLAGS =  -O -g -e:0x0800 -bfunc_lit:0x18.0x800 -dram_end:0xdf -bdata:0x60.0xdf -dhwstk_size:30 -beeprom:0.128 -fihx_coff -S2
FILES = MAIN.o SEGDRV.o 

HEATERSW:	$(FILES)
	$(CC) -o HEATERSW $(LFLAGS) @HEATERSW.lk  
MAIN.o: .\..\..\PHYSIC~1\SW\main.h C:\PROGRA~2\iccavr\include\iot26v.h C:\PROGRA~2\iccavr\include\macros.h C:\PROGRA~2\iccavr\include\AVRdef.h C:\PROGRA~2\iccavr\include\eeprom.h .\..\..\PHYSIC~1\SW\SEGDRV.H C:\PROGRA~2\iccavr\include\stdlib.h C:\PROGRA~2\iccavr\include\_const.h C:\PROGRA~2\iccavr\include\limits.h
MAIN.o:	..\..\PHYSIC~1\SW\MAIN.C
	$(CC) -c $(CFLAGS) ..\..\PHYSIC~1\SW\MAIN.C
SEGDRV.o: .\..\..\PHYSIC~1\SW\segdrv.h C:\PROGRA~2\iccavr\include\iot26v.h C:\PROGRA~2\iccavr\include\stdlib.h C:\PROGRA~2\iccavr\include\_const.h C:\PROGRA~2\iccavr\include\limits.h
SEGDRV.o:	..\..\PHYSIC~1\SW\SEGDRV.C
	$(CC) -c $(CFLAGS) ..\..\PHYSIC~1\SW\SEGDRV.C
